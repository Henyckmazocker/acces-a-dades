package Classe.Program;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.PropertyException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class Main {

	static Institut ins;
	static JSONArray obj;

	public static void main(String[] args) {

		ins = readXML.main();
		obj = jsonReader.main();

		for (String s : textReader.profes) {
			System.out.println(s);
		}

		addProfe("A, B C", textReader.profes);

		for (String s : textReader.profes) {
			System.out.println(s);
		}

		removeProfe("A, B C", textReader.profes);

		for (String s : textReader.profes) {
			System.out.println(s);
		}

		addAl(ins);
		
		removeProfe("A, B C", textReader.main());

	}

	public static void addProfe(String profe, ArrayList<String> list) {

		list.add(profe);
		Collections.sort(list);

		try {

			File f = new File("professors.txt.txt");
			FileWriter fw = new FileWriter(f);
			BufferedWriter bw = new BufferedWriter(fw);

			for (String s : list) {
				bw.write(s);
				bw.newLine();
			}
			bw.flush();
			bw.close();
			fw.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void removeProfe(String profe, ArrayList<String> list) {

		list.remove(profe);

		try {

			File f = new File("professors.txt.txt");
			FileWriter fw = new FileWriter(f);
			BufferedWriter bw = new BufferedWriter(fw);

			for (String s : list) {
				bw.write(s);
				bw.newLine();
			}
			bw.flush();
			bw.close();
			fw.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void addTel(String nom, String tel) {

		for (Alumne a : ins.alumnes) {

			if (a.nom.equals(nom)) {

				a.telefons.add(tel);

			}

		}

	}

	public static void alCarrer(String nom) {

		for (Alumne a : ins.alumnes) {

			if (a.nom.equals(nom)) {

				ins.alumnes.remove(a);

			}

		}

		writeXML();

	}

	public static void comprarMaquina(String aula, String maquina, String pro, boolean graf, JSONArray obj) {

		JSONObject obj2 = null;

		obj2.put("aula", aula);
		obj2.put("maquina", maquina);
		obj2.put("pro", pro);
		obj2.put("graf", graf);

		obj.add(obj2);

	}

	public static void writeJson(JSONArray obj) {

		try (FileWriter file = new FileWriter("classes.json")) {

			file.write(obj.toJSONString());
			file.flush();

		} catch (IOException e) {
			e.printStackTrace();
		}

		System.out.print(obj);

	}

	public static void canviaMaquina(String nomMaquina, String nomAula) {

		JSONObject o3 = null;

		for (Object o : obj) {

			JSONObject o1 = (JSONObject) o;

			JSONArray j1 = (JSONArray) o1.get("maquines");

			for (Object o2 : j1) {

				if (o3.get("nom").equals(nomMaquina)) {

					o3 = (JSONObject) o2;

					j1.remove(o3);

					break;

				}

			}

			if (o3 == null) {
			} else {
				break;
			}

		}

		for (Object o : obj) {

			JSONObject o1 = (JSONObject) o;

			if (o1.get("nom").equals(nomAula)) {

				JSONArray j1 = (JSONArray) o1.get("maquines");

				j1.add(o3);

			}

		}

		writeJson(obj);

	}

	public static void switchAC(String nomAula) {

		for (Object o : obj) {

			JSONObject o1 = (JSONObject) o;

			if(o1.get("nom").equals(nomAula)) {
				
				Boolean  o2 = (Boolean) o1.get("aireacondicionat");
				
				o2 = !o2;
				
			}
			
		}
		
		writeJson(obj);

	}
	
	public static void crearClasse() {
		
		
		
	}

	public static void writeXML() {

		try {

			JAXBContext contextObj = JAXBContext.newInstance(Institut.class);

			Marshaller marshallerObj = contextObj.createMarshaller();
			marshallerObj.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			marshallerObj.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");

			FileOutputStream fos = new FileOutputStream("alumnes.xml");

			marshallerObj.marshal(ins, fos);

		} catch (PropertyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void addAl(Institut ins) {

		ArrayList<String> tel = new ArrayList<String>();

		tel.add("fasfwf");
		tel.add("asfasf");

		ins.alumnes.add(new Alumne("a", "b", "c", "s", tel));

		writeXML();

	}

}
