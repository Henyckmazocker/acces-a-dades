package Classe.Program;

import java.io.Serializable;
import java.util.ArrayList;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlElementWrapper;

import org.w3c.dom.Node;

@XmlRootElement(name = "alumne")
public class Alumne implements Cloneable, Serializable{

	String nom;
	String cognom;
	String DNI;
	String adreca;
	ArrayList<String> telefons = new ArrayList<String>();
	String mail;

	Alumne() {
		super();
	}
	Alumne(String nom, String cognom, String adreca, String mail, ArrayList<String> telefons) {
		super();
		this.nom = nom;
		this.cognom = cognom;
		this.adreca = adreca;
		this.mail = mail;
		this.telefons = telefons;

	}
	
	@XmlElement (name = "DNI")
	public String getDNI() {
		return DNI;
	}
	public void setDNI(String dNI) {
		DNI = dNI;
	}

	@XmlElement (name = "nom")
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	@XmlElement (name = "cognoms")
	public String getCognom() {
		return cognom;
	}

	public void setCognom(String cognom) {
		this.cognom = cognom;
	}

	@XmlElement (name = "adreca")
	public String getAdreca() {
		return adreca;
	}

	public void setAdreca(String adreca) {
		this.adreca = adreca;
	}

	@XmlElementWrapper (name = "telefons")
	@XmlElement (name = "telefon")
	public ArrayList<String> getTelefons() {
		return telefons;
	}

	public void setTelefons(ArrayList<String> telefons) {
		this.telefons = telefons;
	}

	@XmlElement (name = "mail")
	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

}
