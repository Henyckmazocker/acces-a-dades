package Classe.Program;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

import org.json.simple.JSONObject;


public class Classe implements Serializable {

	static JSONObject aula;
	static String profe;
	static ArrayList<Alumne> al = new ArrayList<Alumne>(5);

	public static void main(String[] args) {

		aula = (JSONObject) jsonReader.main().get(0);

		profe = (String) textReader.main().get(0);

		for (int i = 0; i < al.size(); i++) {

			int r = (int) Math.random() * (5 - 0 + 1) + 0;

			al.add(Main.ins.alumnes.get(r));

		}

		deSerialize();

	}

	public static void serialize() {

		FileOutputStream fos;
		try {
			fos = new FileOutputStream("Classe.data");
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(aula);
			oos.writeObject(profe);
			oos.writeObject(al);
			System.out.println("Guardado");
			
			oos.flush();
			oos.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void deSerialize() {

		FileInputStream fis;
		
		try {
			
			fis = new FileInputStream("Classe.data");

			ObjectInputStream ois = new ObjectInputStream(fis);
			
			while (true) {
				
				Object o = ois.readObject();
				
				if (o instanceof JSONObject) {
					
					aula = (JSONObject) o;
					
				}else {
					if(o instanceof String) {
						
						profe = o.toString();
						
						System.out.println(o.toString());
						
					}else {
						if(o instanceof ArrayList) {
							
							ArrayList<?> ao = (ArrayList<?>) o;
							if(ao.isEmpty()) {

							continue;
								
							}else {
								
							Object so = ao.get(0);
							if (so instanceof Alumne) al = (ArrayList<Alumne>) ao;
							
							}
							
						}
					}
				}
					
			}

		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
		}

	}
}
