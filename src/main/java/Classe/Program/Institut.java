package Classe.Program;

import java.io.Serializable;
import java.util.ArrayList;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlElementWrapper;

@XmlRootElement(name = "Institut")
public class Institut implements Serializable{

	String nom;
	ArrayList<Alumne> alumnes = new ArrayList<Alumne>();
	


	public Institut() {
		super();
	}
	
	public Institut(String nom, ArrayList<Alumne> alumnes) {
		
		this.alumnes = alumnes;
		
	}
	
	@XmlElement(name = "nom")
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
	
	@XmlElement(name = "alumne")
	@XmlElementWrapper(name = "alumnes")
	public ArrayList<Alumne> getAlumnes() {
		return alumnes;
	}

	public void setAlumnes(ArrayList<Alumne> alumnes) {
		this.alumnes = alumnes;
	}
	
}
