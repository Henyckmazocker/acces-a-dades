package Classe.Program;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;

public class textReader {
	
	public static ArrayList<String> profes = new ArrayList<String>();


	public static ArrayList main() {
		
		
		try {
			
			File f = new File("professors.txt.txt");
			
			FileReader fr = new FileReader(f);
			BufferedReader br = new BufferedReader(fr);
			
			while(br.ready()) {
				
				String s  = br.readLine();
				
				profes.add(s);
				
			}
			fr.close();
			br.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return profes;
		
		
	}
	
}
