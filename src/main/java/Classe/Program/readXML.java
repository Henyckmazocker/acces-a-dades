package Classe.Program;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.w3c.dom.Node;

public class readXML {

	public static Institut main() {
		
		Institut ins = null;

		try {
			File file = new File("alumnes.xml");
			
			JAXBContext jaxbContext = JAXBContext.newInstance(Institut.class);
			
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			
			ins = (Institut) jaxbUnmarshaller.unmarshal(file);
			
		} catch (JAXBException e) {

			e.printStackTrace();
		}
		
		return ins;

	}
}
